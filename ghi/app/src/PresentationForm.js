import React from 'react';

class PresentationForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            presenter_name: '',
            presenter_email: '',
            company_name: '',
            title: '',
            synopsis: '',
            conference: '',
            conferences: [],};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
        const value = event.target.value;
        this.setState({[event.target.name]: value});

    }


    async handleSubmit(event){
        event.preventDefault();
        const value = { ...this.state};
        console.log(value);
        delete value.conferences;

        const conferencepk = value.conference;
        const presentationUrl = `http://localhost:8000${conferencepk}presentations/`;
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(value),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const presentationResponse = await fetch(presentationUrl, fetchOptions);
        if(presentationResponse.ok) {
            this.setState({
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
            });
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          this.setState({ conferences: data.conferences });
        }
      }

  render() {
    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new presentation</h1>
              <form onSubmit={this.handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                  <input onChange={this.handleChange} value={this.state.presenter_name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                  <label htmlFor="name">Presenter name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleChange} value={this.state.presenter_email} placeholder="Presenter email" required type="text" name="presenter_email" id="presenter_email" className="form-control"/>
                  <label htmlFor="email">Presenter email</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleChange} value={this.state.company_name} placeholder="Company name" required type="text" name="company_name" id="company_name" className="form-control"/>
                  <label htmlFor="company">Company name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleChange} value={this.state.title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                  <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                  <label htmlFor="Synopsis" className="form-label">Synopsis</label>
                  <textarea onChange={this.handleChange} value={this.state.synopsis} className="form-control" name="synopsis" id="synopsis" rows="3"></textarea>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleChange} value={this.state.conference} required  id="conference" name="conference" className="form-select">
                    <option value="">Choose a conference</option>
                    {this.state.conferences.map( conference => {
                        return (
                            <option key={conference.href} value={conference.href}>
                                {conference.name}
                            </option>
                        )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PresentationForm;
